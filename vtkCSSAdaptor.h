#ifndef vtkCSSAdaptor_h
#define vtkCSSAdaptor_h
/*!\file */

#include "vtkCSS.h"
#include "vtkObject.h"
#include "vtkSmartPointer.h"

class vtkCSSStyle;
class vtkDataAssembly;
class vtkPartitionedDataSetCollection;

/// An adaptor for VTK objects to use for querying cascading style
/// sheet (CSS) properties given a document object model (DOM).
///
/// The DOM is defined as a tuple holding a vtkDataAssembly and a
/// vtkPartitionedDataSetCollection. In particular, the nodes of
/// the vtkDataAssembly are akin to HTML tags; they may have
/// attributes.
class vtkCSSAdaptor : public vtkObject
{
public:
  vtkTypeMacro(vtkCSSAdaptor, vtkObject);

  /// The DOM to which CSS applies is a combination of a
  /// vtkDataAssembly and a vtkPartitionedDataSetCollection.
  struct Document
  {
    vtkSmartPointer<vtkDataAssembly> Hierarchy;
    vtkSmartPointer<vtkPartitionedDataSetCollection> DataSets;

    bool operator == (const Document& other) const
    {
      return
        this->Hierarchy == other.Hierarchy &&
        this->DataSets == other.DataSets;
    }
  };

  /// Add a style sheet given its metadata and contents.
  ///
  /// The \a style contents must be a UTF-8 string
  /// holding valid CSS rules.
  ///
  /// The returned integer may be used to remove it.
  /// If -1 is returned, the style sheet was not added.
  /// This can occur because of encoding or syntax errors.
  virtual int AddStyleSheet(
    const std::string& style,
    const std::string& title,
    const std::string& uri,
    css::Origin origin = css::Origin::User,
    css::Importance importance = css::Importance::Normal
  ) = 0;

  /// Remove the specified style sheet.
  /// Returns true if removal actually occurred.
  virtual bool RemoveStyleSheet(int sheetId) = 0;

  /// Return the number of style sheets held by the adaptor.
  virtual int GetNumberOfStyleSheets() const = 0;

  /// Set the type of media which ComputeStyle() should target.
  virtual bool SetMedia(css::Media medium);

  /// Set the assembly and data that make up the document.
  /// Returns true if this object was modified; false otherwise.
  virtual bool SetDocument(const Document& dom);

  /// Compute a set of properties (a style) for a given DOM node.
  virtual vtkSmartPointer<vtkCSSStyle> ComputeStyle(int nodeId, bool composed = true) = 0;

protected:
  Document DOM;
  css::Media Medium = css::Media::Screen;
};

#endif // vtkCSSAdaptor_h
