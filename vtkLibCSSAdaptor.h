#ifndef vtkLibCSSAdaptor_h
#define vtkLibCSSAdaptor_h
/*!\file */
#include "vtkCSSAdaptor.h"

#include <map> // for style sheet map
#include <memory> // for unique_ptr

// Forward declarations for libcss:
struct css_select_handler;
struct css_select_ctx;
struct css_stylesheet;
struct css_media;

/// A CSS adaptor based on netsurf's libcss and libwapcaplet.
class vtkLibCSSAdaptor : public vtkCSSAdaptor
{
public:
  vtkTypeMacro(vtkLibCSSAdaptor, vtkCSSAdaptor);
  static vtkLibCSSAdaptor* New();
  void PrintSelf(ostream& os, vtkIndent indent) override;

  int AddStyleSheet(
    const std::string& style,
    const std::string& title,
    const std::string& uri,
    css::Origin origin,
    css::Importance importance
  ) override;
  bool RemoveStyleSheet(int sheetId) override;
  int GetNumberOfStyleSheets() const override;

  vtkSmartPointer<vtkCSSStyle> ComputeStyle(int nodeId, bool composed = true) override;

protected:
  friend class vtkLibCSSStyle;

  vtkLibCSSAdaptor();
  ~vtkLibCSSAdaptor() override;

  css_select_handler* GetHandler() const;
  std::unique_ptr<css_media> ComputeMedia() const;

  css_select_ctx* SelectionContext;
  std::map<int, css_stylesheet*> Sheets;
};


#endif // vtkLibCSSAdaptor_h
