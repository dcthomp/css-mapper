#include "vtkLibCSSAdaptor.h"
#include "vtkLibCSSStyle.h"

#include "vtkDataAssembly.h"
#include "vtkObjectFactory.h"
#include "vtkPartitionedDataSetCollection.h"
#include "vtkPartitionedDataSet.h"

#include <cctype>

#include <stdio.h>
#include <string.h>

#include <libcss/libcss.h>

vtkStandardNewMacro(vtkLibCSSAdaptor);

extern "C"
{

using Document = vtkCSSAdaptor::Document;

css_error node_name(void* pw, void* n, css_qname* qname)
{
  auto* doc = static_cast<Document*>(pw);
  if (!doc)
  {
    return CSS_INVALID;
  }
  int* nodeId = static_cast<int*>(n);
  if (!nodeId)
  {
    return CSS_BADPARM;
  }
  const char* nodeNameC = doc->Hierarchy->GetNodeName(*nodeId);
  if (!nodeNameC)
  {
    return CSS_BADPARM;
  }
	lwc_string* result;
  lwc_error err = lwc_intern_string(
    nodeNameC, strlen(nodeNameC), &result);
  if (err == lwc_error_ok)
  {
    qname->name = result;
    return CSS_OK;
  }
	return CSS_BADPARM;
}

css_error node_classes(void *pw, void *n,
  lwc_string ***classes, uint32_t *n_classes)
{
  auto* doc = static_cast<Document*>(pw);
  if (!doc)
  {
    return CSS_INVALID;
  }
  int* nodeId = static_cast<int*>(n);
  if (!nodeId)
  {
    return CSS_BADPARM;
  }

  // If data exists at a given node, add the dataset classname (downcased and
  // without the leading "vtk") as the first class.
  std::vector<unsigned int> dataIds = doc->Hierarchy->GetDataSetIndices(*nodeId, false);
  if (!dataIds.empty())
  {
    // All dataset indices for a single node should share some common subclass of vtkDataObject.
  }
  std::vector<std::string> classList;
  if (doc->Hierarchy->GetClasses(*nodeId, classList))
  {
    classes = static_cast<lwc_string***>(calloc(classList.size(), sizeof(lwc_string*)));
    int ii = 0;
    lwc_string* result;
    for (const auto& className : classList)
    {
      lwc_intern_string(className.c_str(), className.size(), &result);
      (*classes)[ii] = result;
      ++ii;
    }
    *n_classes = static_cast<uint32_t>(classList.size());
  }
  else
  {
    *classes = nullptr;
    *n_classes = 0;
  }
	return CSS_OK;
}

css_error node_id(void *pw, void *n, lwc_string **id)
{
  auto* doc = static_cast<Document*>(pw);
  if (!doc)
  {
    return CSS_INVALID;
  }
  int* nodeId = static_cast<int*>(n);
  if (!nodeId)
  {
    return CSS_BADPARM;
  }
  if (doc->Hierarchy->HasAttribute(*nodeId, "id"))
  {
    const char* attributeValue;
    if (
      doc->Hierarchy->GetAttribute(*nodeId, "id", attributeValue) &&
      !attributeValue && !attributeValue[0])
    {
      lwc_string* result;
      lwc_error err = lwc_intern_string(
        attributeValue, strlen(attributeValue), &result);
      if (err == lwc_error_ok)
      {
        *id = result;
        return CSS_OK;
      }
    }
  }
  *id = nullptr;
	return CSS_OK;
}

/// TODO: URL resolution is really an application-level construct.
/// This method exists to resolve URLs from relative to absolute,
/// but **not** to fetch content.
css_error resolve_url(void *pw,
		const char *base, lwc_string *rel, lwc_string **abs)
{
	(void)pw;
	(void)base;

	/* About as useless as possible */
	*abs = lwc_string_ref(rel);

	return CSS_OK;
}

/// Return the ancestor (n-th parent) that has a given \a qname (or null).
css_error named_ancestor_node(void *pw, void *n,
		const css_qname* qname,
		void** ancestor)
{
  auto* doc = static_cast<Document*>(pw);
  if (!doc)
  {
    return CSS_INVALID;
  }
  int* nodeId = static_cast<int*>(n);
  if (!nodeId)
  {
    return CSS_BADPARM;
  }
  *ancestor = nullptr;
  if (*nodeId == vtkDataAssembly::GetRootNode())
  {
    return CSS_OK;
  }
  static thread_local int anode;
  // TBD: Use qname->ns as well.
  std::string name = lwc_string_data(qname->name);
  for (
    anode = doc->Hierarchy->GetParent(*nodeId);
    anode >= 0 && !*ancestor;
    anode = doc->Hierarchy->GetParent(anode)
  )
  {
    if (doc->Hierarchy->GetNodeName(anode) == name)
    {
      *ancestor = &anode;
    }
  }
	return CSS_OK;
}

/// Return the immediate parent if and only if it has the given \a qname.
css_error named_parent_node(void *pw, void *n,
		const css_qname *qname,
		void **parent)
{
  auto* doc = static_cast<Document*>(pw);
  if (!doc)
  {
    return CSS_INVALID;
  }
  int* nodeId = static_cast<int*>(n);
  if (!nodeId)
  {
    return CSS_BADPARM;
  }
  if (*nodeId == vtkDataAssembly::GetRootNode())
  {
    *parent = nullptr;
    return CSS_OK;
  }
  static thread_local int pnode;
  // TBD: Use qname->ns as well.
  std::string name = lwc_string_data(qname->name);
  pnode = doc->Hierarchy->GetParent(*nodeId);
  *parent = (doc->Hierarchy->GetNodeName(pnode) == name ? &pnode : nullptr);
	return CSS_OK;
}

/// Return a "prior" (in traversal order) sibling of the given name (or null).
///
/// This exists for finding precomputed styles stored as user data on nodes.
/// As far as I can tell, the difference between named_generic_sibling_node
/// and named_sibling_node is that generic allows any prior sibling, not
/// just the immediately adjacent one.
/// See [here](https://developer.mozilla.org/en-US/docs/Web/CSS/General_sibling_combinator)
/// for why.
css_error named_generic_sibling_node(void *pw, void *n,
		const css_qname *qname,
		void **sibling)
{
	(void)pw;
	(void)n;
	(void)qname;
	*sibling = NULL;
	return CSS_OK;
}

css_error named_sibling_node(void *pw, void *n,
		const css_qname *qname,
		void **sibling)
{
	(void)pw;
	(void)n;
	(void)qname;
	*sibling = NULL;
	return CSS_OK;
}

/// Return the immediate parent node.
css_error parent_node(void *pw, void *n, void **parent)
{
  auto* doc = static_cast<Document*>(pw);
  if (!doc)
  {
    return CSS_INVALID;
  }
  int* nodeId = static_cast<int*>(n);
  if (!nodeId)
  {
    return CSS_BADPARM;
  }
  if (*nodeId == vtkDataAssembly::GetRootNode())
  {
    *parent = nullptr;
    return CSS_OK;
  }
  static thread_local int pnode;
  pnode = doc->Hierarchy->GetParent(*nodeId);
  *parent = &pnode;
	return CSS_OK;
}

/// Return the immediate-preceding sibling node.
css_error sibling_node(void *pw, void *n, void **sibling)
{
  auto* doc = static_cast<Document*>(pw);
  if (!doc)
  {
    return CSS_INVALID;
  }
  int* nodeId = static_cast<int*>(n);
  if (!nodeId)
  {
    return CSS_BADPARM;
  }
  *sibling = nullptr;
  // Only non-root nodes can have siblings:
  if (*nodeId != 0)
  {
    int parent = doc->Hierarchy->GetParent(*nodeId);
    if (parent >= 0)
    {
      int idx = doc->Hierarchy->GetChildIndex(parent, *nodeId);
      // Only non-first children have preceding siblings:
      if (idx > 0)
      {
        static thread_local int snode;
        snode = doc->Hierarchy->GetChild(parent, idx - 1);
        *sibling = &snode;
      }
    }
  }
	return CSS_OK;
}

css_error node_has_name(void *pw, void *n,
		const css_qname *qname,
		bool *match)
{
  auto* doc = static_cast<Document*>(pw);
  if (!doc)
  {
    return CSS_INVALID;
  }
  int* nodeId = static_cast<int*>(n);
  if (!nodeId)
  {
    return CSS_BADPARM;
  }
  const char* nodeName = doc->Hierarchy->GetNodeName(*nodeId);
	lwc_string* internedName;
  lwc_error err = lwc_intern_string(nodeName, strlen(nodeName), &internedName);
  if (err == lwc_error_ok)
  {
    lwc_string_caseless_isequal(qname->name, internedName, match);
    lwc_string_unref(internedName);
  }
  else
  {
    *match = false;
  }
	return CSS_OK;
}

css_error node_has_class(void *pw, void *n,
		lwc_string *name,
		bool *match)
{
  auto* doc = static_cast<Document*>(pw);
  if (!doc)
  {
    return CSS_INVALID;
  }
  int* nodeId = static_cast<int*>(n);
  if (!nodeId)
  {
    return CSS_BADPARM;
  }
  const char* className = lwc_string_data(name);
	*match = doc->Hierarchy->HasClass(*nodeId, className);
	return CSS_OK;
}

css_error node_has_id(void *pw, void *n,
		lwc_string *name,
		bool *match)
{
  auto* doc = static_cast<Document*>(pw);
  if (!doc)
  {
    return CSS_INVALID;
  }
  int* nodeId = static_cast<int*>(n);
  if (!nodeId)
  {
    return CSS_BADPARM;
  }
  const char* idAttr;
  if (doc->Hierarchy->GetAttribute(*nodeId, "id", idAttr) && idAttr && idAttr[0])
  {
    lwc_string* internedId;
    lwc_error err = lwc_intern_string(idAttr, strlen(idAttr), &internedId);
    if (err == lwc_error_ok)
    {
      lwc_string_isequal(name, internedId, match);
      lwc_string_unref(internedId);
    }
    else
    {
      *match = false;
    }
  }
  else
  {
    *match = false;
  }
	return CSS_OK;
}

css_error node_has_attribute(void *pw, void *n,
		const css_qname *qname,
		bool *match)
{
  auto* doc = static_cast<Document*>(pw);
  if (!doc)
  {
    return CSS_INVALID;
  }
  int* nodeId = static_cast<int*>(n);
  if (!nodeId)
  {
    return CSS_BADPARM;
  }
  *match = false;
  const char* name = lwc_string_data(qname->name);
  if (doc->Hierarchy->HasAttribute(*nodeId, name))
  {
    *match = true;
  }
  return CSS_OK;
}

css_error node_has_attribute_equal(void *pw, void *n,
		const css_qname *qname,
		lwc_string *value,
		bool *match)
{
  auto* doc = static_cast<Document*>(pw);
  if (!doc)
  {
    return CSS_INVALID;
  }
  int* nodeId = static_cast<int*>(n);
  if (!nodeId)
  {
    return CSS_BADPARM;
  }
  *match = false;
  const char* name = lwc_string_data(qname->name);
  if (doc->Hierarchy->HasAttribute(*nodeId, name))
  {
    const char* attributeValue;
    if (
      doc->Hierarchy->GetAttribute(*nodeId, name, attributeValue) &&
      !attributeValue && !attributeValue[0])
    {
      lwc_string* attribute;
      lwc_error err = lwc_intern_string(
        attributeValue, strlen(attributeValue), &attribute);
      if (err == lwc_error_ok)
      {
        lwc_string_isequal(attribute, value, match);
        lwc_string_unref(attribute);
      }
    }
  }
  return CSS_OK;
}

css_error node_has_attribute_dashmatch(void *pw, void *n,
		const css_qname *qname,
		lwc_string *value,
		bool *match)
{
	(void)pw;
	(void)n;
	(void)qname;
	(void)value;
	*match = false;
	return CSS_OK;
}

css_error node_has_attribute_includes(void *pw, void *n,
		const css_qname *qname,
		lwc_string *value,
		bool *match)
{
	(void)pw;
	(void)n;
	(void)qname;
	(void)value;
	*match = false;
	return CSS_OK;
}

css_error node_has_attribute_prefix(void *pw, void *n,
		const css_qname *qname,
		lwc_string *value,
		bool *match)
{
	(void)pw;
	(void)n;
	(void)qname;
	(void)value;
	*match = false;
	return CSS_OK;
}

css_error node_has_attribute_suffix(void *pw, void *n,
		const css_qname *qname,
		lwc_string *value,
		bool *match)
{
	(void)pw;
	(void)n;
	(void)qname;
	(void)value;
	*match = false;
	return CSS_OK;
}

css_error node_has_attribute_substring(void *pw, void *n,
		const css_qname *qname,
		lwc_string *value,
		bool *match)
{
	(void)pw;
	(void)n;
	(void)qname;
	(void)value;
	*match = false;
	return CSS_OK;
}

css_error node_is_first_child(void *pw, void *n, bool *match)
{
  auto* doc = static_cast<Document*>(pw);
  if (!doc)
  {
    return CSS_INVALID;
  }
  int* nodeId = static_cast<int*>(n);
  if (!nodeId)
  {
    return CSS_BADPARM;
  }
  if (*nodeId == 0)
  {
    // Root node is not first child.
    *match = false;
  }
  else
  {
    int parent = doc->Hierarchy->GetParent(*nodeId);
    *match = (parent < 0 ? false : doc->Hierarchy->GetChildIndex(parent, *nodeId) == 0);
  }
	return CSS_OK;
}

css_error node_is_root(void *pw, void *n, bool *match)
{
	(void)pw;
  int* nodeId = static_cast<int*>(n);
  if (!nodeId)
  {
    return CSS_BADPARM;
  }
	*match = (*nodeId == 0);
	return CSS_OK;
}

css_error node_count_siblings(void *pw, void *n,
		bool same_name, bool after, int32_t *count)
{
  auto* doc = static_cast<Document*>(pw);
  if (!doc)
  {
    return CSS_INVALID;
  }
  int* nodeId = static_cast<int*>(n);
  if (!nodeId)
  {
    return CSS_BADPARM;
  }
  if (*nodeId == 0)
  {
    // Root node has no siblings.
    *count = 0;
  }
  else
  {
    int parent = doc->Hierarchy->GetParent(*nodeId);
    int numChildren = doc->Hierarchy->GetNumberOfChildren(parent);
    if (!same_name && !after)
    {
      *count = static_cast<int32_t>(numChildren);
    }
    else
    {
      int32_t numSiblings = 0;
      std::string nodeName = doc->Hierarchy->GetNodeName(*nodeId);
      int begin = (after ? doc->Hierarchy->GetChildIndex(parent, *nodeId) : 0);
      if (after && !same_name)
      {
        *count = numChildren - begin - 1;
      }
      else
      { // same_name == true
        for (int idx = begin; idx < numChildren; ++idx)
        {
          if (nodeName == doc->Hierarchy->GetNodeName(doc->Hierarchy->GetChild(parent, idx)))
          {
            ++numSiblings;
          }
        }
        *count = numSiblings;
      }
    }
  }
	return CSS_OK;
}

css_error node_is_empty(void *pw, void *n, bool *match)
{
  auto* doc = static_cast<Document*>(pw);
  if (!doc)
  {
    return CSS_INVALID;
  }
  int* nodeId = static_cast<int*>(n);
  if (!nodeId)
  {
    return CSS_BADPARM;
  }
	*match = doc->Hierarchy->GetNumberOfChildren(*nodeId) == 0;
	return CSS_OK;
}

/// We need to understand what a "link" is for VTK.
css_error node_is_link(void *pw, void *n, bool *match)
{
	(void)pw;
	(void)n;
	*match = false;
	return CSS_OK;
}

/// We need to understand what "visited" means for VTK.
css_error node_is_visited(void *pw, void *n, bool *match)
{
	(void)pw;
	(void)n;
	*match = false;
	return CSS_OK;
}

/// Implementing this requires interactor feedback which is not part of the mapper.
css_error node_is_hover(void *pw, void *n, bool *match)
{
	(void)pw;
	(void)n;
	*match = false;
	return CSS_OK;
}

/// Implementing this requires interactor feedback which is not part of the mapper.
css_error node_is_active(void *pw, void *n, bool *match)
{
	(void)pw;
	(void)n;
	*match = false;
	return CSS_OK;
}

/// Implementing this requires interactor feedback which is not part of the mapper
/// as well as understanding conceptually how focus might be manipulated.
/// Is data focused by keyboard navigation? A QAbstractItemView? Last mouse click?
css_error node_is_focus(void *pw, void *n, bool *match)
{
	(void)pw;
	(void)n;
	*match = false;
	return CSS_OK;
}

/// We need to understand what "enabled" means for VTK.
/// The data is pickable?
css_error node_is_enabled(void *pw, void *n, bool *match)
{
	(void)pw;
	(void)n;
	*match = false;
	return CSS_OK;
}

/// We need to understand what "disabled" means for VTK.
/// The data is not pickable?
css_error node_is_disabled(void *pw, void *n, bool *match)
{
	(void)pw;
	(void)n;
	*match = false;
	return CSS_OK;
}

/// We need to understand what "checked" means for VTK.
css_error node_is_checked(void *pw, void *n, bool *match)
{
	(void)pw;
	(void)n;
	*match = false;
	return CSS_OK;
}

/// We need to understand what being a "target" means for VTK.
css_error node_is_target(void *pw, void *n, bool *match)
{
	(void)pw;
	(void)n;
	*match = false;
	return CSS_OK;
}


/// This doesn't make sense at the moment (not until we render text/labels).
css_error node_is_lang(void *pw, void *n,
		lwc_string *lang,
		bool *match)
{
	(void)pw;
	(void)n;
	(void)lang;
	*match = false;
	return CSS_OK;
}

/// Really, this should be avoided.
/// Since a vtkDataAssembly defines arbitrary elements
/// (unlike HTML), style should not exist in attributes.
/// Keep it in CSS style sheets. If this is really needed
/// for some arcane reason, a plugin relevant to the
/// assembly's content should provide this method.
css_error node_presentational_hint(void *pw, void *node,
		uint32_t *nhints, css_hint **hints)
{
	(void)pw;
	(void)node;
	*nhints = 0;
	*hints = NULL;
	return CSS_OK;
}

/// TODO: defaults for all rendering properties.
css_error ua_default_for_property(void *pw, uint32_t property, css_hint *hint)
{
	(void)pw;

	if (property == CSS_PROP_COLOR) {
		hint->data.color = 0x00000000;
		hint->status = CSS_COLOR_COLOR;
	} else if (property == CSS_PROP_FONT_FAMILY) {
		hint->data.strings = NULL;
		hint->status = CSS_FONT_FAMILY_SANS_SERIF;
	} else if (property == CSS_PROP_QUOTES) {
		/* Not exactly useful :) */
		hint->data.strings = NULL;
		hint->status = CSS_QUOTES_NONE;
	} else if (property == CSS_PROP_VOICE_FAMILY) {
		/** \todo Fix this when we have voice-family done */
		hint->data.strings = NULL;
		hint->status = 0;
	} else {
		return CSS_INVALID;
	}

	return CSS_OK;
}

css_error compute_font_size(void *pw, const css_hint *parent, css_hint *size)
{
	static css_hint_length sizes[] = {
		{ FLTTOFIX(6.75), CSS_UNIT_PT },
		{ FLTTOFIX(7.50), CSS_UNIT_PT },
		{ FLTTOFIX(9.75), CSS_UNIT_PT },
		{ FLTTOFIX(12.0), CSS_UNIT_PT },
		{ FLTTOFIX(13.5), CSS_UNIT_PT },
		{ FLTTOFIX(18.0), CSS_UNIT_PT },
		{ FLTTOFIX(24.0), CSS_UNIT_PT }
	};
	const css_hint_length *parent_size;

	(void)pw;

	/* Grab parent size, defaulting to medium if none */
	if (parent == NULL) {
		parent_size = &sizes[CSS_FONT_SIZE_MEDIUM - 1];
	} else {
		assert(parent->status == CSS_FONT_SIZE_DIMENSION);
		assert(parent->data.length.unit != CSS_UNIT_EM);
		assert(parent->data.length.unit != CSS_UNIT_EX);
		parent_size = &parent->data.length;
	}

	assert(size->status != CSS_FONT_SIZE_INHERIT);

	if (size->status < CSS_FONT_SIZE_LARGER) {
		/* Keyword -- simple */
		size->data.length = sizes[size->status - 1];
	} else if (size->status == CSS_FONT_SIZE_LARGER) {
		/** \todo Step within table, if appropriate */
		size->data.length.value =
				FMUL(parent_size->value, FLTTOFIX(1.2));
		size->data.length.unit = parent_size->unit;
	} else if (size->status == CSS_FONT_SIZE_SMALLER) {
		/** \todo Step within table, if appropriate */
		size->data.length.value =
				FMUL(parent_size->value, FLTTOFIX(1.2));
		size->data.length.unit = parent_size->unit;
	} else if (size->data.length.unit == CSS_UNIT_EM ||
			size->data.length.unit == CSS_UNIT_EX) {
		size->data.length.value =
			FMUL(size->data.length.value, parent_size->value);

		if (size->data.length.unit == CSS_UNIT_EX) {
			size->data.length.value = FMUL(size->data.length.value,
					FLTTOFIX(0.6));
		}

		size->data.length.unit = parent_size->unit;
	} else if (size->data.length.unit == CSS_UNIT_PCT) {
		size->data.length.value = FDIV(FMUL(size->data.length.value,
				parent_size->value), FLTTOFIX(100));
		size->data.length.unit = parent_size->unit;
	}

	size->status = CSS_FONT_SIZE_DIMENSION;

	return CSS_OK;
}

static css_error set_libcss_node_data(void *pw, void *n, void *libcss_node_data);

static css_error get_libcss_node_data(void *pw, void *n,
		void **libcss_node_data)
{
  auto* doc = static_cast<Document*>(pw);
  if (!doc)
  {
    return CSS_INVALID;
  }
  int* nodeId = static_cast<int*>(n);
  if (!nodeId)
  {
    return CSS_BADPARM;
  }
	*libcss_node_data = doc->Hierarchy->GetNodeUserData(*nodeId);
	return CSS_OK;
}

/* Table of function pointers for the LibCSS Select API. */
static css_select_handler select_handler = {
	CSS_SELECT_HANDLER_VERSION_1,

	node_name,
	node_classes,
	node_id,
	named_ancestor_node,
	named_parent_node,
	named_sibling_node,
	named_generic_sibling_node,
	parent_node,
	sibling_node,
	node_has_name,
	node_has_class,
	node_has_id,
	node_has_attribute,
	node_has_attribute_equal,
	node_has_attribute_dashmatch,
	node_has_attribute_includes,
	node_has_attribute_prefix,
	node_has_attribute_suffix,
	node_has_attribute_substring,
	node_is_root,
	node_count_siblings,
	node_is_empty,
	node_is_link,
	node_is_visited,
	node_is_hover,
	node_is_active,
	node_is_focus,
	node_is_enabled,
	node_is_disabled,
	node_is_checked,
	node_is_target,
	node_is_lang,
	node_presentational_hint,
	ua_default_for_property,
	compute_font_size,
	set_libcss_node_data,
	get_libcss_node_data
};

static css_error set_libcss_node_data(void *pw, void *n,
		void *libcss_node_data)
{
  auto* doc = static_cast<Document*>(pw);
  if (!doc)
  {
    return CSS_INVALID;
  }
  int* nodeId = static_cast<int*>(n);
  if (!nodeId)
  {
    return CSS_BADPARM;
  }

  auto handler =
    [pw](
      void* input, vtkDataAssembly::NodeEvent event,
      vtkDataAssembly* src, int srcId,
      vtkDataAssembly* dst, int dstId) -> void*
  {
    css_error error;
    switch (event)
    {
    case vtkDataAssembly::ANCESTOR_MODIFIED:
      // Abandon optimizations rather than recompute.
      error = css_libcss_node_data_handler(&select_handler, CSS_NODE_DELETED,
        pw, &srcId, NULL, input);
      input = nullptr;
      break;
    case vtkDataAssembly::MODIFIED:
      error = css_libcss_node_data_handler(&select_handler, CSS_NODE_MODIFIED,
        pw, &srcId, NULL, input);
      break;
    case vtkDataAssembly::REMOVED:
      css_libcss_node_data_handler(&select_handler, CSS_NODE_DELETED,
        pw, &srcId, NULL, input);
      input = nullptr;
      break;
    case vtkDataAssembly::CLONED:
      if (src != dst || srcId == dstId)
      {
        // Do not clone across different assemblies or to self.
        return nullptr;
      }
      error = css_libcss_node_data_handler(&select_handler, CSS_NODE_CLONED,
        pw, &srcId, &dstId, input);
      break;
    };
    return input;
  };

  if (!doc->Hierarchy->SetNodeUserData(*nodeId, libcss_node_data, handler))
  {
    // Since we're not storing it, delete node data immediately.
    css_libcss_node_data_handler(&select_handler, CSS_NODE_DELETED,
      pw, n, NULL, libcss_node_data);
  }

	return CSS_OK;
}

} // extern "C"

vtkLibCSSAdaptor::vtkLibCSSAdaptor()
{
	css_error code = css_select_ctx_create(&this->SelectionContext);
	if (code != CSS_OK)
  {
    vtkErrorMacro("Could not create context.");
  }
}

vtkLibCSSAdaptor::~vtkLibCSSAdaptor()
{
  for (const auto& entry : this->Sheets)
  {
    this->RemoveStyleSheet(entry.first);
  }
	css_error code = css_select_ctx_destroy(this->SelectionContext);
	if (code != CSS_OK)
  {
    vtkErrorMacro("Could not destroy context.");
  }
}

void vtkLibCSSAdaptor::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  os << indent << "SelectionContext: " << this->SelectionContext << "\n";
  os << indent << "Sheets: " << this->Sheets.size() << "\n";
}

int vtkLibCSSAdaptor::AddStyleSheet(
  const std::string& style,
  const std::string& title,
  const std::string& uri,
  css::Origin origin,
  css::Importance importance
)
{
	css_stylesheet_params params{
	  .params_version = CSS_STYLESHEET_PARAMS_VERSION_1,
	  .level = CSS_LEVEL_21,
	  .charset = "UTF-8",
	  .url = uri.c_str(),
	  .title = title.c_str(), 
	  .allow_quirks = false,
	  .inline_style = false,
	  .resolve = resolve_url,
	  .resolve_pw = nullptr,
	  .import = nullptr,
	  .import_pw = nullptr,
	  .color = nullptr,
	  .color_pw = nullptr,
	  .font = nullptr,
	  .font_pw = nullptr,
  };
	css_stylesheet* sheet;
	css_error code;
  code = css_stylesheet_create(&params, &sheet);
	if (code != CSS_OK || !sheet)
  {
		vtkErrorMacro("css_stylesheet_create failed: " << code);
    return -1;
  }
  int sheetId = this->GetNumberOfStyleSheets() + 1;
  this->Sheets[sheetId] = sheet;

  /*
  size_t size;
	code = css_stylesheet_size(sheet, &size);
	printf("created stylesheet, size %zu\n", size);
  */

	// Parse the CSS source.
	code = css_stylesheet_append_data(
    sheet, reinterpret_cast<const uint8_t *>(style.c_str()), style.size());
	if (code != CSS_OK && code != CSS_NEEDDATA)
  {
    vtkErrorMacro("Could not append data (check for encoding and syntax errors). " << code);
    this->RemoveStyleSheet(sheetId);
    return -1;
  }
	code = css_stylesheet_data_done(sheet);
  // NB: If code == CSS_IMPORTS_PENDING, then we must load the stylesheet
  //     which may be asynchronous (for remote URLs). See nscss_register_import
  //     in netsurf's content/handlers/css/css.c.
	if (code != CSS_OK)
  {
    vtkErrorMacro("Could not finalize stylesheet import. " << code);
    this->RemoveStyleSheet(sheetId);
    return -1;
  }

  /*
	code = css_stylesheet_size(sheet, &size);
	printf("appended data, size now %zu\n", size);
  */

  code = css_select_ctx_append_sheet(
    this->SelectionContext, sheet, static_cast<css_origin>(origin),
      nullptr);
  if (code != CSS_OK)
  {
    vtkErrorMacro("Could not add sheet to adaptor (" << code <<" ).");
    this->RemoveStyleSheet(sheetId);
    return -1;
  }

  return sheetId;
}

bool vtkLibCSSAdaptor::RemoveStyleSheet(int sheetId)
{
  bool didRemove = false;
  auto it = this->Sheets.find(sheetId);
  if (it != this->Sheets.end())
  {
    css_error code;
    code = css_stylesheet_destroy(it->second);
    if (code != CSS_OK)
    {
      vtkErrorMacro("Failed to destroy stylesheet (" << code << ")");
    }
    this->Sheets.erase(it);
    didRemove = true;
  }
  return didRemove;
}

int vtkLibCSSAdaptor::GetNumberOfStyleSheets() const
{
  if (this->Sheets.empty())
  {
    return 0;
  }
  return this->Sheets.rbegin()->first;
  // return static_cast<int>(this->Sheets.size());
}

vtkSmartPointer<vtkCSSStyle> vtkLibCSSAdaptor::ComputeStyle(int nodeId, bool composed)
{
  vtkSmartPointer<vtkCSSStyle> style;
  style.TakeReference(new vtkLibCSSStyle(this, nodeId, composed));
  return style;
}

css_select_handler* vtkLibCSSAdaptor::GetHandler() const
{
  return &select_handler;
}

std::unique_ptr<css_media> vtkLibCSSAdaptor::ComputeMedia() const
{
  auto media = std::make_unique<css_media>();
  switch (this->Medium)
  {
  case css::Media::Screen:
    media->type = CSS_MEDIA_SCREEN;
    break;
  case css::Media::Print:
    media->type = CSS_MEDIA_PRINT;
    break;
  }
  // TODO: Fill out resolution, width, height, etc. by 
  //       accepting from Renderer/RenderWindow via Mapper?
  return media;
}
