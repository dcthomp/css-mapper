#ifndef vtkCSSStyle_h
#define vtkCSSStyle_h
/*!\file */

#include "vtkCSS.h" // For enums
#include "vtkObject.h"
#include "vtkVector.h" // For vtkVector[234]d properties.

/// Object for querying a computed style.
///
/// These objects are created and owned by a vtkCSSAdaptor.
/// They may be invalidated by the adaptor.
/// An example of when invalidation occurs is when the document,
/// media, or style sheets are updated.
/// In general, you should not hold style objects but rather
/// compute them as needed.
class vtkCSSStyle : public vtkObject
{
public:
  vtkTypeMacro(vtkCSSStyle, vtkObject);

  virtual bool IsValid() const = 0;
  virtual bool GetComputedColor(
    vtkVector4d& rgba, bool& isInherited, css::PseudoElement element = css::PseudoElement::None) = 0;
  // virtual bool GetLengthProperty(const std::string& name, const std::string& units, double& value);
  // virtual bool GetFrequencyProperty(const std::string& name, const std::string& units, double& value);
  // virtual bool GetAngleProperty(const std::string& name, const std::string& units, double& value);
};

#endif // vtkCSSStyle_h
