#ifndef vtkCSS_h
#define vtkCSS_h
/*!\file */

/// A namespace for cascading style sheet enums and free functions.
namespace css
{

/// Style sheets may be provided by different originating parties.
/// The type of party determines the precedence of the style rules,
/// along with the css::Importance and the specificity of each
/// rule's selector.
///
/// WARNING: The enums are ordered to match libcss (although libcss
/// does not include animation or transition origins).
enum class Origin
{
  UserAgent, //!< Settings from application or browser for a consistent UI.
  User,      //!< User-provided preferences.
  Author,    //!< Style that comes from the content producer.
  Animation, //!< Properties being animated
  Transition //!< Properties undergoing a transition animation.
};

/// Style sheets and rules may be provided with an importance
/// that allows them to change the precedence of style rules.
enum class Importance
{
  Normal,
  Important
};

/// The medium of communication with the user.
///
/// Style rules may specify media and properties
/// (such as aspect ratios) of the media to
/// restrict their scope.
/// Reponsive design in particular uses the
/// resolution and aspect ratio to ensure that
/// style "responds" to the device on which it
/// is rendered. Examples include changing layout
/// when a device switches from portrait to
/// landscape mode, or when an external monitor
/// with different resolution is attached.
enum class Media
{
  Screen, //!< A backlit computer screen
  Print   //!< Paper with ink
};

/// Pseudo-elements allow selectors to differentiate
/// style on elements whose position is significant in some way,
/// and in some cases, append or replace content to some elements.
enum class PseudoElement
{
  None,        //!< The element whose style is requested is not positionally significant.
  FirstLine,   //!< The element whose style is requested is the first line of text.
  FirstLetter, //!< The element whose style is requested is the first letter of text.
  Before,      //!< The style or content that should appear before a given element.
  After,       //!< The style or content that should appear after a given element.

  Count        //!< The number of pseudo-element types.
};

/// Pseudo-classes allow selectors to differentiate style based
/// on the _state_ of an element (as opposed to pseudo-classes that
/// modify style/content based on the _position_ of an element).
enum class PseudoClass
{
  None,
  // Language-specific
  Dir,
  Lang,
  // Link-specific
  AnyLink,
  Link,
  Visited,
  LocalLink,
  Target,
  TargetWithin,
  Scope,
  // User action
  Hover,
  Active,
  Focus,
  FocusVisible,
  FocusWithin,
  // Time-specific
  Current,
  Past,
  Future,
  // Video- or animation-specific
  Playing,
  Paused,
  // Input elements
  Autofill,
  Enabled,
  Disabled,
  ReadOnly,
  ReadWrite,
  PlaceholderShown,
  Default,
  Checked,
  Indeterminant,
  Blank,
  Valid,
  Invalid,
  InRange,
  OutOfRange,
  Required,
  Optional,
  UserInvalid,
  // Tree-structure-specific
  Root,
  Empty,
  NthChild,
  NthLastChild,
  FirstChild,
  LastChild,
  OnlyChild,
  NthOfType,
  NthLastOfType,
  FirstOfType,
  LastOfType,
  OnlyOfType,

  Count            //!< The number of pseudo-classes.
};

} // css namespace

#endif // vtkCSS_h
