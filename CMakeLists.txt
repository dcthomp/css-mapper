cmake_minimum_required(VERSION 3.19)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
project(css-mapper)

#list(APPEND CMAKE_PREFIX_PATH "/stage/build/misc/install") # NB: PkgConfig stuff appends "lib/pkgconfig" to this.

find_package(PkgConfig REQUIRED)
pkg_check_modules(LIBWAPCAPLET IMPORTED_TARGET REQUIRED libwapcaplet)
pkg_check_modules(LIBPARSERUTILS IMPORTED_TARGET REQUIRED libparserutils)
pkg_check_modules(LIBCSS IMPORTED_TARGET REQUIRED libcss)

find_package(Boost 1.64.0 COMPONENTS date_time filesystem system REQUIRED)

# find_package(libwapcaplet REQUIRED)
# find_package(libparserutils REQUIRED)
# find_package(libcss REQUIRED)
find_package(VTK REQUIRED
  COMPONENTS
    CommonCore
    CommonDataModel
    CommonExecutionModel
    FiltersSources
    InteractionStyle
    RenderingCore
    RenderingOpenGL2
)

find_package(pegtl REQUIRED)
find_package(ICU REQUIRED COMPONENTS i18n uc data)

set(classes
  # Should be in VTK/RenderingCore
  vtkCSSAdaptor
  vtkCSSStyle
  # vtkDataAssemblyMapper

  # Should be in VTK/Rendering/LibCSS
  vtkLibCSSAdaptor
  vtkLibCSSStyle
)
set(headers
  vtkCSS.h
)
set(sources)
foreach(class ${classes})
  list(APPEND headers ${class}.h)
  set(source "${class}.cxx")
  if (EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/${source}")
    list(APPEND sources ${source})
  endif()
endforeach()
add_library(vtkcss
  ${sources}
  ${headers}
)
target_link_libraries(vtkcss
  PUBLIC
    VTK::CommonCore
    VTK::CommonDataModel
    VTK::InteractionStyle
    VTK::RenderingCore
    VTK::RenderingOpenGL2
    PkgConfig::LIBCSS
    PkgConfig::LIBPARSERUTILS
    PkgConfig::LIBWAPCAPLET
)

add_executable(demo demo.cxx)
target_link_libraries(demo
  VTK::CommonCore
  VTK::CommonDataModel
  VTK::CommonExecutionModel
  VTK::FiltersSources
  VTK::RenderingCore
  VTK::RenderingOpenGL2
  vtkcss
)

add_executable(pegtl-demo pegtl-demo.cxx)
target_link_libraries(pegtl-demo
  VTK::CommonCore
  VTK::CommonDataModel
  VTK::CommonExecutionModel
  VTK::RenderingCore
  VTK::RenderingOpenGL2
  taocpp::pegtl
  ICU::i18n
  ICU::data
  ICU::uc
  Boost::boost
)

add_executable(better better.cxx)
target_link_libraries(better
  VTK::CommonCore
  VTK::CommonDataModel
  VTK::CommonExecutionModel
  VTK::RenderingCore
  VTK::RenderingOpenGL2
  taocpp::pegtl
  ICU::i18n
  ICU::data
  ICU::uc
  Boost::boost
)
