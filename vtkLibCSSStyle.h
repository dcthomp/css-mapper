#ifndef vtkLibCSSStyle_h
#define vtkLibCSSStyle_h
/*!\file */

#include "vtkCSSStyle.h"

struct css_select_results;

class vtkLibCSSAdaptor;

/// Object for querying a computed style.
class vtkLibCSSStyle : public vtkCSSStyle
{
public:
  vtkTypeMacro(vtkLibCSSStyle, vtkCSSStyle);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  bool IsValid() const override;
  bool GetComputedColor(vtkVector4d& rgba, bool& isInherited, css::PseudoElement pseudoElement) override;
  // virtual bool GetLengthProperty(const std::string& name, const std::string& units, double& value);
  // virtual bool GetFrequencyProperty(const std::string& name, const std::string& units, double& value);
  // virtual bool GetAngleProperty(const std::string& name, const std::string& units, double& value);

protected:
  friend class vtkLibCSSAdaptor;
  vtkLibCSSStyle();
  vtkLibCSSStyle(vtkLibCSSAdaptor* adaptor, int node, bool composed);
  ~vtkLibCSSStyle() override;

  void Invalidate();

  vtkLibCSSAdaptor* Adaptor;
  int Node; // The DOM node to which this style applies
  css_select_results* Style;
};

#endif // vtkLibCSSStyle_h
