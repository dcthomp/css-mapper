#include "vtkCSSAdaptor.h"

#include "vtkDataAssembly.h"
#include "vtkPartitionedDataSetCollection.h"

bool vtkCSSAdaptor::SetMedia(css::Media medium)
{
  if (medium == this->Medium)
  {
    return false;
  }
  this->Medium = medium;
  this->Modified();
  // TODO: Trigger an update for all extant Style objects to
  //       compute new properties?
  return true;
}

bool vtkCSSAdaptor::SetDocument(const Document& dom)
{
  if (dom == this->DOM)
  {
    return false;
  }
  this->DOM = dom;
  this->Modified();
  return true;
}
