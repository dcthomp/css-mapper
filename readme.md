# Demo of CSS-based rendering attributes

The rendering process for the demo includes these steps:
+ Read/create a vtkPartitionedDataSetCollection and one or more vtkDataAssembly objects.
+ Read/create one or more style sheets.
+ Compute styles for each node in the assembly.
+ Read and/or compute the top-level document style.
  The most significant is the default draw style (surface, wireframe, feature-edges, etc.)
  and default color mode.
+ Perhaps we create a derived vtkPartitionedDataSetCollection that contains transformed
  datasets for rendering/picking? Otherwise, we would have to write shaders that do things
  like extract unstructured-grid surfaces or feature-edges (or silhouette edges, which we totally should support).
+ Identify groups of nodes with the same style and dataset-type (in the transformed collection).
+ Create command and data buffers for each group of like-styled nodes (in parallel would be nice).
+ TODO: How to handle CSS animations/tweens?

When rendering occurs, we:
+ Update timers for animations/effects.
+ Start timers for performance measurement.
+ Invoke command buffers on data.
+ Stop timers for performance and update anticipated framerate for animations.

## Questions

1. How to handle fencing/blocking of buffers and VTK data for parallel rendering and processing?
2. How to handle "scenes" where effects like ambient occlusion or shadows require renders to
   (private, intermediate) framebuffers bound as textures to other nodes? The vtkPartitionedDataSetCollection
   handles data well, but vtkDataAssembly is not really geared to deal with multipass rendering to
   multiple targets in stereo, etc. We could certainly add new vtkDataObject subclasses for render-passes
   and make these things explicit, but that would definitely require transformation of the input
   data collection (with no scene markup) into a renderable representation.
3. Picking? Hovering? Gestures?


## CSS properties for VTK data objects

Rendering is controlled with CSS properties.
The following properties are all used at times,
but be aware that some properties affect which other properties are used.

+ `color-by`: run-time enum (enumerated values may be extended by plugins)
  + `array` (in which case `color-array` is used)
  + `solid` (in which case `color` is used)
  + `texture` (in which case `texture` and optionally `color` are used)
  + `shader` (in which case a custom shader, specified with `shader`, is used)

+ `render-style`: run-time enum (enumerated values may be extended by plugins)
  + `surface`: extract the boundary surface and render its primitives
  + `edges`: extract the boundary surface, then extract the surface's edges, then render those primitives.
  + `wireframe`: extract all edges of the dataset, then render those primitives.
  + `silhouette`: render edges whose face-normals dotted with the camera view-vector have different signs.

+ TBD: `render-target`: the name of a framebuffer to target. If null, then the render-window's framebuffer is used.

+ `color-array`: composite property holding
  + `color-array-centering`: enum
    + `point`: colors specified on points
    + `cell`: colors specificed on cells
    + `row`: colors specified on table rows
    + `node`: colors specified on graph nodes
    + `edge`: colors specified on graph arcs
    + `field`: solid color for the entire dataset obtained from field-data
  + `color-array-name`: string
  + `color-array-transfer`: enum
    + `channels`: a comma-separated list enclosed in parentheses of the following values (one per array component).
      Note that some of the enums below may not be combined with others (e.g., red/green/blue and cyan/magenta/yellow/k).
      Warnings will be emitted if invalid combinations are used.
      + `hue`: the frequency of color along a spectrum. If unspecified, defaults to 0 (red)
      + `saturation`: the fraction of intensity specific to color versus black (value/intensity) or white (lightness/brightness)
      + `brightness`: the amount of white to be blended with color
      + `lightness` (a.k.a. `luminance`): the amount of white to be blended with color
      + `intensity`: the amount of black to be blended with color
      + `value`: the amount of black to be blended with color
      + `red`: the fractional intensity of red in the color's spectrum
      + `green`: the fractional intensity of green in the color's spectrum
      + `blue`: the fractional intensity of blue in the color's spectrum
      + `opacity`: 0 for fully transparent, 1 for fully opaque
      + `cyan`: the fractional presence of cyan in the "pixel ink"
      + `magenta`: the fractional presence of magenta in the "pixel ink"
      + `yellow`: the fractional presence of yellow in the "pixel ink"
      + `k`: the fractional presence of black in the "pixel ink"
    + `lookup`: indicates the `color-lookup` property should be used to map array values to colors

+ `color`: a web color (hex string or standard named color) to apply to a target data object
  Only used when `color-by` is set to `solid` or `texture`/`shader` (in which case it is passed as a uniform value to the shader).

+ `texture`: a composite property holding
  + `texture-coordinates-array`
  + `texture-clamping`
  + `texture-interpolation`
  + `texture-image`


