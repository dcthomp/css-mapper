#include "vtkPartitionedDataSetCollection.h"
#include "vtkPartitionedDataSet.h"
#include "vtkDataAssembly.h"
// #include "vtkDataAssemblyMapper.h"
// #include "vtkCSSAdaptor.h"
#include "vtkLibCSSAdaptor.h"
#include "vtkCSSStyle.h"
#include "vtkSmartPointer.h"
#include "vtkNew.h"
#include "vtkCubeSource.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"

#include <cctype>

#include <stdio.h>
#include <string.h>

#include <libcss/libcss.h>

namespace {

// Create some example content and style for the mapper.
void createDemoData(
  vtkDataAssembly* hierarchy,
  vtkPartitionedDataSetCollection* datasets,
  std::string& authorCSS,
  int mm,
  int nn
)
{
  vtkNew<vtkCubeSource> cs;
  cs->SetXLength(0.5);
  cs->SetYLength(0.5);
  cs->SetZLength(0.5);
  hierarchy->SetRootNodeName("body");
  int table = hierarchy->AddNode("table");
  int rows = hierarchy->AddNode("rows", table);
  datasets->SetNumberOfPartitionedDataSets(mm * nn);
  for (int jj = 0; jj < nn; ++jj)
  {
    int row = hierarchy->AddNode("row", rows);
    if (jj == 0)
    {
      hierarchy->SetAttribute(row, "id", "favoriteRow");
    }
    for (int ii = 0; ii < mm; ++ii)
    {
      cs->SetCenter(ii + 0.5, jj + 0.5, 0.0);
      cs->Update();
      vtkNew<vtkPolyData> cube;
      vtkNew<vtkPartitionedDataSet> pd;
      cube->DeepCopy(cs->GetOutputDataObject(0));
      pd->SetNumberOfPartitions(1);
      pd->SetPartition(0, cube);
      unsigned int idx = ii + mm * jj;
      datasets->SetPartitionedDataSet(idx, pd);
      int cell = hierarchy->AddNode("cell", row);
      hierarchy->AddDataSetIndex(cell, idx);
    }
  }
  // The content producer (i.e., the "author")
  // may provide style hints with data. The user
  // and user agent may override it.
  authorCSS = R"css(
    table { color: grey; border: 3px; }
    rows { color: #a9a9a9; }
    #favoriteRow { color: #be0000; }
    row:nth-child(even) { color: #e0e0e0; }
    row:nth-child(odd) { color: #dedb3f; }
    cell { color: inherit; }
    h1 { color: #aaa }
		h4 { color: #321; }
		h4, h5 { color: #123456; }
  )css";
}

} // anonymous namespace

int main(int argc, char* argv[])
{
  vtkNew<vtkRenderWindow> window;
  vtkNew<vtkRenderer> renderer;
  vtkNew<vtkActor> actor;
  vtkNew<vtkRenderWindowInteractor> interactor;

  window->AddRenderer(renderer);
  renderer->AddActor(actor);

  vtkNew<vtkDataAssembly> hierarchy;
  vtkNew<vtkPartitionedDataSetCollection> datasets;
  std::string authorCSS;
  createDemoData(hierarchy, datasets, authorCSS, 4, 3);

  vtkNew<vtkLibCSSAdaptor> adaptor;
  adaptor->SetDocument({hierarchy, datasets});
  // vtkNew<vtkDataAssemblyMapper> mapper;
  // actor->SetMapper(mapper);
  // mapper->SetInputDataObject(0, datasets);
  // mapper->SetAssembly(hierarchy);
  // Whatever generates content (createDemoData in this example) can
  // also provide style. Its origin should be "author" so that style
  // cascades can work properly.
  adaptor->AddStyleSheet(
    authorCSS,
    /* title */ "demo", /* URI */ "app://demo.css",
    css::Origin::Author, css::Importance::Normal
  );
  if (argc > 1)
  {
    std::ifstream readCSS(argv[1]);
    std::string userCSS(std::istreambuf_iterator<char>{readCSS}, {});
    // Users (and user agents, such as the browser application) can
    // also provide style. User style should be marked as such so
    // it can be prioritized property. Here, a file provided by the
    // user on the command line is used as an example user style.
    adaptor->AddStyleSheet(
      userCSS,
      /* title */ "user", /* URI */ "app://user.css",
      css::Origin::User, css::Importance::Normal
    );
  }

  adaptor->SetMedia(css::Media::Screen);

  int nodeId = 0;
	for (int nodeId = 0; nodeId < 18; ++nodeId)
  {
    auto style = adaptor->ComputeStyle(nodeId);
    bool inherited;
    vtkVector4d rgba;
    style->GetComputedColor(rgba, inherited);

    std::cout << "color of " << hierarchy->GetNodeName(nodeId) << " (" << nodeId << ") is " << rgba;
    if (inherited)
    {
      std::cout << " (inherited)";
    }
    std::cout << "\n";
	}

  // window->Render();
  // interactor->Initialize();
  // interactor->Start();

#if 0
	css_error code;
	css_stylesheet *sheet;
	size_t size;
  Document doc(3,2);
	const char data[] =
    "table { color: grey; border: 3px; }"
    "rows { color: #a9a9a9; }"
    "#favoriteRow { color: #be0000; }"
    "row:nth-child(even) { color: #e0e0e0; }"
    "row:nth-child(odd) { color: #dedb3f; }"
    "cell { color: inherit; }"
    "h1 { color: #aaa } "
		"h4 { color: #321; } "
		"h4, h5 { color: #123456; }";
	css_select_ctx* select_ctx;
	uint32_t count;
	unsigned int hh;
	css_stylesheet_params params;
	css_media media = {
		.type = CSS_MEDIA_SCREEN,
	};

	params.params_version = CSS_STYLESHEET_PARAMS_VERSION_1;
	params.level = CSS_LEVEL_21;
	params.charset = "UTF-8";
	params.url = "file:demo.cxx";
	params.title = "foo";
	params.allow_quirks = false;
	params.inline_style = false;
	params.resolve = resolve_url;
	params.resolve_pw = NULL;
	params.import = NULL;
	params.import_pw = NULL;
	params.color = NULL;
	params.color_pw = NULL;
	params.font = NULL;
	params.font_pw = NULL;

	/* create a stylesheet */
	code = css_stylesheet_create(&params, &sheet);
	if (code != CSS_OK)
		die("css_stylesheet_create", code);
	code = css_stylesheet_size(sheet, &size);
	if (code != CSS_OK)
		die("css_stylesheet_size", code);
	printf("created stylesheet, size %zu\n", size);


	/* parse some CSS source */
	code = css_stylesheet_append_data(sheet, (const uint8_t *) &data,
			sizeof data);
	if (code != CSS_OK && code != CSS_NEEDDATA)
		die("css_stylesheet_append_data", code);
	code = css_stylesheet_data_done(sheet);
  // NB: If code == CSS_IMPORTS_PENDING, then we must load the stylesheet
  //     which may be asynchronous (for remote URLs). See nscss_register_import
  //     in netsurf's content/handlers/css/css.c.
	if (code != CSS_OK)
		die("css_stylesheet_data_done", code);
	code = css_stylesheet_size(sheet, &size);
	if (code != CSS_OK)
		die("css_stylesheet_size", code);
	printf("appended data, size now %zu\n", size);


	/* prepare a selection context containing the stylesheet */
	code = css_select_ctx_create(&select_ctx);
	if (code != CSS_OK)
		die("css_select_ctx_create", code);
	code = css_select_ctx_append_sheet(select_ctx, sheet, CSS_ORIGIN_AUTHOR,
			NULL);
	if (code != CSS_OK)
		die("css_select_ctx_append_sheet", code);
	code = css_select_ctx_count_sheets(select_ctx, &count);
	if (code != CSS_OK)
		die("css_select_ctx_count_sheets", code);
	printf("created selection context with %i sheets\n", count);


	/* select style for each of h1 to h6 */
  const char* elements[] = { "body", "table", "rows", "row", "cell" };
  int nodeId = 0;
	for (hh = 0; hh < 5; ++hh, ++nodeId)
  {
		css_select_results* style;
		char element[20];
		// lwc_string* element_name;
		uint8_t color_type;
		css_color color_shade;

		// lwc_intern_string(elements[hh], strlen(elements[hh]), &element_name);
		code = css_select_style(select_ctx, &nodeId,
				&media, nullptr,
				&select_handler, &doc,
				&style);
		if (code != CSS_OK)
			die("css_select_style", code);

		// lwc_string_unref(element_name);

		color_type = css_computed_color(
				style->styles[CSS_PSEUDO_ELEMENT_NONE],
				&color_shade);
		if (color_type == CSS_COLOR_INHERIT)
			printf("color of %s is 'inherit'\n", elements[nodeId]);
		else
			printf("color of %s is %x\n", elements[nodeId], color_shade);

		code = css_select_results_destroy(style);
		if (code != CSS_OK)
			die("css_computed_style_destroy", code);
	}


	/* free everything and shut down libcss */
	code = css_select_ctx_destroy(select_ctx);
	if (code != CSS_OK)
		die("css_select_ctx_destroy", code);
	code = css_stylesheet_destroy(sheet);
	if (code != CSS_OK)
		die("css_stylesheet_destroy", code);

	return 0;
#endif
}
