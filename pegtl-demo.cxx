#define TAO_PEGTL_NAMESPACE css_pegtl
#include "tao/pegtl.hpp"
#define DBG_GRAMMAR 1
#ifdef DBG_GRAMMAR
#  include "tao/pegtl/analyze.hpp"
#endif
#include "tao/pegtl/contrib/icu/utf8.hpp"
#define DBG_PARSE 1

#include "TypeName.h"

#include <memory>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <typeinfo>

namespace css
{

namespace rule = tao::css_pegtl;

namespace token
{

struct CDO : rule::keyword<'<', '!', '-', '-'>
{
};

struct CDC : rule::keyword<'-', '-', '>'>
{
};

struct colon : rule::keyword<':'>
{
};

struct semicolon : rule::keyword<';'>
{
};

struct comma : rule::keyword<','>
{
};

struct bracket_open : rule::keyword<'['>
{
};

struct bracket_close : rule::keyword<']'>
{
};

struct paren_open : rule::keyword<'('>
{
};

struct paren_close : rule::keyword<')'>
{
};

struct curly_open : rule::keyword<'{'>
{
};

struct curly_close : rule::keyword<'}'>
{
};

struct charset : rule::plus<rule::ascii::ranges<0x0, 0x21, 0x23, 0x7f>>
{
};

struct encoding :
  rule::seq<
    rule::string<'@', 'c', 'h', 'a', 'r', 's', 'e', 't', ' ', '"'>,
    charset,
    rule::string<'"', ';', '\n'>
  >
{
};

struct non_ascii : rule::utf8::ranges<0xa0, 0x10ffff>
{
};

struct newline :
  rule::sor<
    rule::string<'\n'>,
    rule::string<'\r', '\n'>,
    rule::string<'\r'>,
    rule::string<'\f'>
  >
{
};

struct whitespace : rule::plus<rule::utf8::icu::white_space>
{
};

struct optional_whitespace :
  rule::opt<token::whitespace>
{
};

struct hex_number : rule::rep_min_max<1, 6, rule::utf8::ranges<'0', '9', 'a', 'f', 'A', 'F'>>
{
};

#if 0
struct hex_number
{
  using rule_t = hex_number;
#ifdef DBG_GRAMMAR
  using analyze_t = tao::css_pegtl::analysis::generic<tao::css_pegtl::analysis::rule_type::ANY>;
#endif

  template<typename ParseInput>
  static bool match(ParseInput& in)
  {
    if (in.empty()) { return false; }
    int count;
    for (count = 0; count < 6; ++count)
    {
      if (!std::isxdigit(in.byte()))
      {
        break;
      }
    }
    bool haveNumber = count > 0 && count <= 6;
    if (haveNumber)
    {
      in.bump(count);
    }
    return haveNumber;
  }
};
#endif

struct escape :
  rule::seq<
    rule::string<'\\'>,
    rule::sor<
      token::hex_number,
      rule::utf8::not_one<'\n', '\r', '\f'>
    >
  >
{
};

struct ident_suffix :
  rule::star<
    rule::sor<
      token::escape,
      rule::utf8::ranges<'a', 'z', 'A', 'Z', '0', '9'>,
      rule::string<'-'>,
      rule::string<'_'>,
      token::non_ascii
    >
  >
{
};


struct ident :
  rule::seq<
    rule::sor<
      rule::string<'-','-'>,
      rule::seq<
        rule::opt<rule::string<'-'>>,
        rule::sor<
          token::escape,
          token::non_ascii,
          rule::utf8::ranges<'a', 'z', 'A', 'Z', '_'>
        >
      >
    >,
    token::ident_suffix
  >
{
};

struct at : rule::seq<rule::string<'@'>, token::ident>
{
};

struct function
{
};

struct at_keyword : rule::keyword<'@'>
{
};

struct hash :
  rule::seq<
    rule::string<'#'>,
    token::ident_suffix
  >
{
};

struct line_continuation :
  rule::seq<
    rule::string<'\\'>,
    token::newline
  >
{
};

template<int Delimiter>
struct unescaped_string_data :
  rule::utf8::not_one<Delimiter, '\\', '\n', '\r', '\f'>
{
};

struct double_quoted_string :
  rule::seq<
    rule::string<'"'>,
    rule::star<
      rule::sor<
        token::unescaped_string_data<'"'>,
        token::escape,
        token::line_continuation
      >
    >,
    rule::string<'"'>
  >
{
};

struct single_quoted_string :
  rule::seq<
    rule::string<'\''>,
    rule::star<
      rule::sor<
        token::unescaped_string_data<'\''>,
        token::escape,
        token::line_continuation
      >
    >,
    rule::string<'\''>
  >
{
};

struct string :
  rule::sor<
    token::double_quoted_string,
    token::single_quoted_string
  >
{
};

struct bad_string
{
};

struct url :
  rule::seq<
    rule::string<'u', 'r', 'l'>,
    token::paren_open,
    token::optional_whitespace,
    rule::opt<
      rule::star<
        rule::sor<
          token::escape,
          rule::utf8::not_one<'"', '\'', '\\', ' '> // FIXME: This should also omit whitespace and non-printable characters.
        >
      >
    >,
    token::optional_whitespace,
    token::paren_close
  >
{
};

struct bad_url
{
};

struct number :
  rule::seq<
    // Sign
    rule::opt<rule::utf8::one<'+', '-'>>,
    // Mantissa
    rule::sor<
      rule::seq<
        rule::plus<rule::ascii::digit>, rule::string<'.'>, rule::plus<rule::ascii::digit>
      >,
      rule::seq<
        rule::plus<rule::ascii::digit>
      >,
      rule::seq<
        rule::string<'.'>, rule::plus<rule::ascii::digit>
      >
    >,
    // Exponent
    rule::opt<
      rule::seq<
        rule::one<'e', 'E'>,
        rule::opt<rule::utf8::one<'+', '-'>>,
        rule::plus<rule::ascii::digit>
      >
    >
  >
{
};

struct percentage :
  rule::seq<
    token::number,
    rule::string<'%'>
  >
{
};

struct dimension :
  rule::seq<
    token::number,
    token::ident
  >
{
};

struct delim :
  rule::utf8::any
{
};

struct any :
  rule::sor<
    rule::seq<rule::at<rule::utf8::icu::white_space>, token::whitespace>,
    rule::seq<rule::at<rule::string<'"'>>, token::double_quoted_string>,
    rule::seq<rule::at<rule::string<'\''>>, token::single_quoted_string>,
    token::hash,
    token::paren_open,
    token::paren_close,
    rule::seq<rule::at<rule::string<'+'>>, token::number>,
    rule::seq<rule::at<rule::string<'+'>>, token::delim>,
    token::comma,
    rule::seq<rule::at<rule::string<'-'>>, token::number>,
    rule::seq<rule::at<rule::string<'-'>>, token::ident>,
    rule::seq<rule::at<rule::string<'-'>>, token::delim>,
    rule::seq<rule::at<rule::string<'.'>>, token::number>,
    rule::seq<rule::at<rule::string<'.'>>, token::delim>,
    token::colon,
    token::semicolon,
    rule::seq<rule::at<rule::string<'<'>>, token::CDO>,
    rule::seq<rule::at<rule::string<'<'>>, token::delim>,
    rule::seq<rule::at<rule::string<'@'>>, token::at>,
    rule::seq<rule::at<rule::string<'@'>>, token::delim>,
    token::bracket_open,
    rule::seq<rule::at<rule::string<'\\'>>, token::ident>,
    rule::seq<rule::at<rule::string<'\\'>>, token::delim>, // parse error
    token::bracket_close,
    token::curly_open,
    token::curly_close,
    token::number,
    token::ident,
    token::delim
  >
{
};

} // token namespace

namespace composite
{

struct comment :
  rule::seq<
    rule::string<'/', '*'>,
    rule::until<rule::utf8::string<'*', '/'>>
  >
{
};

struct bad_comment :
  rule::seq<
    rule::string<'/', '*'>,
    rule::minus<rule::star<rule::utf8::any>, rule::utf8::string<'*', '/'>>,
    rule::eof
  >
{
};

struct component_value_list; // Forward declaration
struct preserved_token; // Forward declaration

struct curly_block :
  rule::seq<
    token::curly_open,
    composite::component_value_list,
    token::curly_close
  >
{
};

struct paren_block :
  rule::seq<
    token::paren_open,
    composite::component_value_list,
    token::paren_close
  >
{
};

struct bracket_block :
  rule::seq<
    token::bracket_open,
    composite::component_value_list,
    token::bracket_close
  >
{
};

struct simple_block : rule::sor<
  composite::curly_block,
  composite::paren_block,
  composite::bracket_block
>
{
};

struct function_block : rule::seq<
  token::ident,
  rule::if_must<token::paren_open, composite::simple_block>
>
{
};

struct component_value :
  rule::sor<
    composite::preserved_token,
    composite::function_block,
    composite::simple_block
  >
{
};

struct component_value_list : rule::list<
  composite::component_value, token::whitespace
>
{
};

struct at_rule :
  rule::seq<
    token::at,
    rule::opt<composite::component_value_list>,
    rule::sor<composite::curly_block, token::semicolon>
  >
{
};


struct qualified_rule : rule::seq<
  rule::opt<composite::component_value_list>,
  token::optional_whitespace,
  composite::curly_block
>
{
};

// Any token produced by the tokenizer except for <function-token>s, <{-token>s, <(-token>s, and <[-token>s.
struct preserved_token :
  rule::sor<
    composite::comment,
    token::newline,
    token::whitespace,
    token::ident,
    token::hex_number,
    token::escape,
    token::at,
    token::hash,
    token::string,
    token::url,
    token::number,
    token::dimension,
    token::percentage,
    token::CDO,
    token::CDC
  >
{
};

} // composite namespace

struct stylesheet
{
  stylesheet() = default;
  bool valid = true;
  std::string encoding = "utf-8";
};

template<typename Rule>
struct action
#if !DBG_PARSE
  : rule::nothing<Rule>
#endif
{
#if DBG_PARSE
  template< typename Input >
  static void apply(
    const Input& in,
    stylesheet& sheet)
  {
    (void)sheet;
    if (
      !std::is_same<Rule, css::token::whitespace>::value &&
      !std::is_same<Rule, rule::star<css::token::whitespace>>::value &&
      !std::is_same<Rule, tao::css_pegtl::utf8::icu::white_space>::value &&
      !std::is_same<Rule, tao::css_pegtl::utf8::any>::value &&
      !std::is_same<Rule, tao::css_pegtl::utf8::any>::value
    )
    {
      std::cout << "Token " << smtk::common::typeName<Rule>() << " match \"" << in.string() << "\"\n";
    }
  }
#endif
};

template<>
struct action<token::charset>
{
  template< typename Input >
  static void apply(
    const Input& in,
    stylesheet& sheet)
  {
    sheet.encoding = in.string();
  }
};

template<>
struct action<token::CDO>
{
  template< typename Input >
  static void apply(
    const Input& in,
    stylesheet& sheet)
  {
    std::cout << "Ignoring CDO\n";
  }
};

template<>
struct action<token::CDC>
{
  template< typename Input >
  static void apply(
    const Input& in,
    stylesheet& sheet)
  {
    std::cout << "Ignoring CDC\n";
  }
};

template<>
struct action<token::ident>
{
  template< typename Input >
  static void apply(
    const Input& in,
    stylesheet& sheet)
  {
    std::cout << "Identifier " << in.string() << "\n";
  }
};

template<>
struct action<composite::comment>
{
  template< typename Input >
  static void apply(
    const Input& in,
    stylesheet& sheet)
  {
    std::cout << "Good comment " << in.string() << "\n";
  }
};

template<>
struct action<composite::bad_comment>
{
  template< typename Input >
  static void apply(
    const Input& in,
    stylesheet& sheet)
  {
    std::cout << "Bad comment " << in.string() << "\n";
  }
};

template<>
struct action<composite::component_value_list>
{
  template< typename Input >
  static void apply(
    const Input& in,
    stylesheet& sheet)
  {
    std::cout << "Component value list \"" << in.string() << "\"\n";
  }
};

template<>
struct action<composite::curly_block>
{
  template< typename Input >
  static void apply(
    const Input& in,
    stylesheet& sheet)
  {
    std::cout << "Curly block \"" << in.string() << "\"\n";
  }
};

struct stylesheet_grammar : rule::seq<
  rule::opt<token::encoding>,
  rule::star<token::any>,
  rule::eof
>
{
};

struct grammar : rule::seq<
  rule::opt<token::encoding>,
  rule::star<
    rule::sor<
      token::whitespace,
      token::CDO,
      token::CDC,
      composite::comment,
      composite::bad_comment,
      composite::qualified_rule,
      composite::at_rule
    >
  >,
  rule::eof
>
{
};

struct source
{
  source() { }
  source(const std::string& name)
    : m_name(name)
  {
  }

  const std::string& name() const { return m_name; }

  std::string m_name;
};

template<typename Eol = tao::css_pegtl::eol::lf_crlf>
struct source_stream : source
{
  using InputType = tao::css_pegtl::istream_input<Eol>;
  source_stream(const std::string& name, std::istream* stream)
    : source(name)
    , m_stream(stream)
    , m_input(*stream, 32768, name)
  {
  }
  ~source_stream()
  {
    delete m_stream;
  }

  tao::css_pegtl::istream_input<Eol>& input() { return m_input; }

  std::istream* m_stream;
  tao::css_pegtl::istream_input<Eol> m_input;
};

template<
  tao::css_pegtl::tracking_mode TrackingMode = tao::css_pegtl::tracking_mode::IMMEDIATE,
  typename Eol = tao::css_pegtl::eol::lf_crlf >
struct source_file : source
{
  using InputType = tao::css_pegtl::read_input<TrackingMode, Eol>;
  source_file(const std::string& filename)
    : source(filename)
  {
    if (filename.empty())
    {
      throw std::invalid_argument("Empty filename not allowed.");
    }
    {
      std::ifstream testAccess(filename);
      if (!testAccess.good())
      {
        throw std::runtime_error("Cannot open file.");
      }
    }
    m_input = new tao::css_pegtl::read_input<TrackingMode, Eol>(filename);
  }
  ~source_file()
  {
    delete m_input;
  }

  tao::css_pegtl::read_input<TrackingMode, Eol>& input() { return *m_input; }

  tao::css_pegtl::read_input<TrackingMode, Eol>* m_input = nullptr;
};

template<typename Error, typename Source, typename = void>
struct error_reporter
{
  error_reporter(Error& e, Source& source)
  {
    std::cerr << e.what() << "\n";
  }
};

// SFINAE partial specialization for css::source_file<...> classes
template<typename Error, typename Source>
struct error_reporter<Error, Source, decltype(Source::InputType::line_at)>
{
  error_reporter(Error& e, Source& source)
  {
    const auto p = e.positions.front();
    std::cerr
      << e.what() << "\n"
      << source.input().line_at(p) << "\n"
      << std::setw( p.byte ) << '^' << "\n";
  }
};

template<typename Source>
std::unique_ptr<css::stylesheet> read_stylesheet(Source& source)
{
  using namespace tao::css_pegtl;

  std::unique_ptr<css::stylesheet> sheet(new css::stylesheet);
  try
  {
    bool valid = parse<css::stylesheet_grammar, css::action>(source.input(), *sheet.get());
    sheet->valid &= valid;
  }
  catch (parse_error& e)
  {
    error_reporter reporter(e, source);
    sheet->valid = false;
  }

  return sheet;
}

} // css namepace

#ifdef DBG_GRAMMAR
namespace tao
{
namespace css_pegtl
{

  /* For PEGTL v3
template< typename Name >
struct analyze_traits< Name, css::token::hex_number >
   : analyze_any_traits<>
{};
*/

} // namespace css_pegtl
} // namespace tao
#endif // DBG_GRAMMAR

int main(int argc, char* argv[])
{
  using namespace tao::css_pegtl;

#ifdef DBG_GRAMMAR
  if (analyze<css::grammar>() != 0 )
  {
    std::cerr << "CSS grammar: cycles without progress detected!\n";
    return 1;
  }
#endif

  std::unique_ptr<css::stylesheet> sheet;
  if (argc > 1)
  {
    std::string filename(argv[1]);
    std::unique_ptr<css::source_file<>> source_file(new css::source_file(filename));
    sheet = css::read_stylesheet(*source_file);
  }
  else
  {
    std::unique_ptr<css::source_stream<>> source_stream(new css::source_stream("stdin", &std::cin));
    sheet = css::read_stylesheet(*source_stream);
  }

  if (sheet->valid)
  {
    std::cout << "Read stylesheet with encoding " << sheet->encoding << "\n";
  }
  else
  {
    std::cerr << "Error parsing sheet.\n";
  }

  return 0;
}
