#include "vtkLibCSSStyle.h"
#include "vtkLibCSSAdaptor.h"

#include "vtkDataAssembly.h"
#include "vtkPartitionedDataSetCollection.h"

#include <libcss/libcss.h>

vtkLibCSSStyle::vtkLibCSSStyle()
  : Adaptor(nullptr)
  , Node(-1)
  , Style(nullptr)
{
}

vtkLibCSSStyle::vtkLibCSSStyle(vtkLibCSSAdaptor* adaptor, int node, bool composed)
  : Adaptor(adaptor)
  , Node(node)
{
  css_stylesheet* inlineStyle = nullptr;
  int inlineSheet = -1;
  const char* inlineStyleAtt;
  if (this->Adaptor->DOM.Hierarchy->GetAttribute(node, "style", inlineStyleAtt))
  {
    inlineSheet = this->Adaptor->AddStyleSheet(
      inlineStyleAtt,
      /* title */ "inline", /* uri */ "app://inline",
    css::Origin::Author, css::Importance::Normal);
    inlineStyle = this->Adaptor->Sheets[inlineSheet];
  }
  auto media = this->Adaptor->ComputeMedia();
  css_error code = css_select_style(
    this->Adaptor->SelectionContext, &this->Node,
    media.get(), inlineStyle,
    this->Adaptor->GetHandler(), &this->Adaptor->DOM,
    &this->Style);
  if (inlineSheet != -1)
  {
    this->Adaptor->RemoveStyleSheet(inlineSheet);
  }
  if (code != CSS_OK)
  {
    vtkErrorMacro("Could not compute style (" << code << ")");
    this->Style = nullptr;
  }

  if (composed)
  {
    auto& dom(this->Adaptor->DOM);
    // Traverse upwards in the tree, composing this node's style with its parent's style.
    if (node != 0)
    {
      int parent = dom.Hierarchy->GetParent(node);
      auto parentStyle = new vtkLibCSSStyle(adaptor, parent, true);
      css_computed_style* composedStyle;
      code = css_computed_style_compose(
        parentStyle->Style->styles[0 /*css::PseudoElement::None*/],
        this->Style->styles[0 /*css::PseudoElement::None*/],
        this->Adaptor->GetHandler()->compute_font_size,
        &dom,
        &composedStyle
      );
      if (code == CSS_OK)
      {
        css_computed_style_destroy(this->Style->styles[CSS_PSEUDO_ELEMENT_NONE]);
        this->Style->styles[CSS_PSEUDO_ELEMENT_NONE] = composedStyle;
      }
      else
      {
        vtkErrorMacro("Could not compose style (" << code << ").");
      }
      parentStyle->Delete();
    }
  }
}

vtkLibCSSStyle::~vtkLibCSSStyle()
{
  // Free the style, if any:
  this->Invalidate();
}

void vtkLibCSSStyle::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  os << indent << "Adaptor: " << this->Adaptor << "\n";
  os << indent << "Node: " << this->Node << "\n";
  os << indent << "Style: " << this->Style << "\n";
}

bool vtkLibCSSStyle::IsValid() const
{
  return !!this->Style;
}

bool vtkLibCSSStyle::GetComputedColor(vtkVector4d& rgba, bool& isInherited, css::PseudoElement pseudoElement)
{
  assert(pseudoElement != css::PseudoElement::Count);

  css_color color_shade;
  uint8_t color_type = css_computed_color(
    this->Style->styles[static_cast<int>(pseudoElement)],
    &color_shade);
  isInherited = (color_type == CSS_COLOR_INHERIT);
  rgba[0] = (color_shade >> 16 & 0xff) / 255.0;
  rgba[1] = (color_shade >>  8 & 0xff) / 255.0;
  rgba[2] = (color_shade       & 0xff) / 255.0;
  rgba[3] = (color_shade >> 24 & 0xff) / 255.0;
  return true;
}

// virtual bool vtkLibCSSStyle::GetLengthProperty(const std::string& name, const std::string& units, double& value);
// virtual bool vtkLibCSSStyle::GetFrequencyProperty(const std::string& name, const std::string& units, double& value);
// virtual bool vtkLibCSSStyle::GetAngleProperty(const std::string& name, const std::string& units, double& value);

void vtkLibCSSStyle::Invalidate()
{
  if (this->Style)
  {
    css_error code = css_select_results_destroy(this->Style);
    if (code != CSS_OK)
    {
      vtkErrorMacro("Could not destroy style (" << code << ")");
    }
    this->Style = nullptr;
  }
}
